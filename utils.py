import math
import time


def nearest_array_position(array, position):
	deltaBest = math.inf
	for i in range(0, len(array)):
		delta = abs(position - array[i][0])
		if delta < deltaBest:
			deltaBest = delta
			positionBest = i
	return positionBest


def interpolate_array_positions(array, position):
    #requires a sorted 2D array of key-value pairs
    indexBelow = 0
    for i in range(0, len(array)):
        if array[i][0] < position:
            indexBelow = i
    lowerValue = array[indexBelow][1]
    lowerIndex = array[indexBelow][0]
    if indexBelow == len(array)-1:
        return lowerValue
    upperValue = array[indexBelow+1][1]
    upperIndex = array[indexBelow+1][0]
    midPoint = lowerValue + (position-lowerIndex)*(upperValue-lowerValue)/(upperIndex-lowerIndex)
    return midPoint


class tictoc():
    def __init__(self):
        self.start = time.time()

    def end(self):
        if self.start == None:
            raise Exception("Cannot end tictoc, has not been started.")
        elapsed = time.time() - self.start
        print("Elapsed time %.3fs." % elapsed)
        return elapsed