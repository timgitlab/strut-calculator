This is a calculator for designing the location and strut force for gas-strut door openers.
It takes strut force and (x,y) positions of components to return net moment. 
The conclusion can then be reached whether force is sufficient to maintain the door's open position.
It is currently a CLI only implementation. 

**CLASSES**
- The 'Location' class assigns the x,y position and contains methods to apply rotations about a hinge(x,y) to return transformed positions.
- The 'Mass' class inherits 'Location' but adds a method to calculate the moment of a mass about the hinge(x,y).
- The 'Strut' class is a linked list of two 'Location' objects (one must be fixed and one free) with some error checking and will calculate strut length, angle and applied moment about hinge(x,y). 
- The 'Handle' class is a special location class that can calculate handle loads from torques and position relative to hinge. This is a key design value.


**MAIN LOOP**
- Currently 'Location', 'Mass', 'Strut' and 'Handle' objects are assigned manually in code, with commented blocks to switch between two options of geometries.
- The while loop iterates over angles, and (while iterating over all 'Strut' and 'Mass' objects) transforms all the free 'Location' objects and calculates net torque about the hinge.
- Torque and angle are stored to a dictionary and then called by a plotting function to graph net torque.


**UTILITIES**
- Strut force as a function of strut length is imported from a csv file.
- Some specific handlers are provided as functions.
- More general handlers are provided in utils.py so I can use them in other projects.

