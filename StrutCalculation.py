import math
import matplotlib.pyplot as plt
import utils
import csv
# import pdb
# pdb.set_trace()

# --------------------FURTHER IDEAS--------------------#
# hinge loads from force components
# temperature impact on force
# multiple strut length inconsistencies
# 3D mount locations

t = utils.tictoc()

g = 9.8  # m/s/s


def dot_product(X, Y):
    result = [[0 for col in range(len(Y[0]))] for row in range(len(X))]
    for i in range(len(X)):
        for j in range(len(Y[0])):
            for k in range(len(Y)):
                result[i][j] += X[i][k] * Y[k][j]
    return result


def rotation(vector, angle):
    theta = angle * math.pi / 180
    # rotation matrix solution in 2D
    R_x = [[math.cos(theta), -math.sin(theta)],
           [math.sin(theta), math.cos(theta)]]
    return dot_product(X=R_x, Y=vector)


def import_csv(filename):
    with open(filename, 'r') as csvfile:
        result = []
        csv_data = csv.reader(csvfile, delimiter=",")
        for row in csv_data:
            col1 = float(row[0])
            col2 = float(row[1])
            result.append([col1, col2])
        return result


def custom_plot(torqueResult, handleForce):
    fig, ax = plt.subplots()
    ax.plot(torqueResult['anglePosition'], torqueResult['netTorque'], label='Net Torque')
    ax.plot(torqueResult['anglePosition'], torqueResult['strutTorque'], label='Strut Torque')
    ax.plot(torqueResult['anglePosition'], torqueResult['COGTorque'], label='Door Torque')
    ax.plot(torqueResult['anglePosition'], handleForce, label='Handle Force')
    plt.title('Torque at angular rotations')
    ax.set_xlabel('Angle (degrees)')
    ax.set_ylabel('Torque (Nm)')
    ax.legend()
    ax.grid()
    ax.set_xlim(0, None)
    plt.show()


class location():
    def __init__(self, x, y, fixed):
        self.x = x
        self.y = y
        self.fixed = fixed

    def __str__(self):
        return f'x = {round(self.x, 3)}, y = {round(self.y, 3)}, part is {self.fixed}.'

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def position(self, hinge, angle):
        if hinge.fixed != 'hinge':
            return "No hinge passed to method."
        if self.fixed == 'fixed':
            return "Cannot compute rotation of fixed attachment."
        if self == hinge:
            return "Attachment and hinge are the same."
        # create a vector treating the hinge as zero point
        vector = [[self.x - hinge.x],
                  [self.y - hinge.y]]
        VectorPrime = rotation(vector=vector, angle=angle)
        # return to an absolute position by re-adding the hinge position
        NewPosition = [[hinge.x + VectorPrime[0][0]],
                       [hinge.y + VectorPrime[1][0]]]
        return NewPosition

    def rotate(self, hinge, angle):
        result = self.position(hinge=hinge, angle=angle)
        self.x = result[0][0]
        self.y = result[1][0]


class Mass(location):
    instances = []

    def __init__(self, x, y, fixed, mass):
        for massIterator in self.instances:
            if massIterator.x == x and massIterator.y == y:
                raise Exception("Attempting to declare duplicate centre of mass.")
        self.x = x
        self.y = y
        self.fixed = fixed
        self.mass = mass
        self.instances.append(self)

    def __str__(self):
        return f'x = {round(self.x, 3)}, y = {round(self.y, 3)}, part is {self.fixed} and has mass {self.mass}.'

    def torque(self, hinge):
        momentArm = self.x - hinge.x
        moment = self.mass * momentArm * g
        return moment


class Handle(location):
    instances = []

    def __init__(self, x, y, fixed):
        if len(self.instances) == 1:
            raise Exception("Attempting to declare second handle.")
        self.x = x
        self.y = y
        self.fixed = fixed
        self.instances.append(self)

    def __str__(self):
        return f'x = {round(self.x, 3)}, y = {round(self.y, 3)}, and part is {self.fixed}.'

    def force(self, hinge, netMoment):
        momentArm = math.sqrt(pow(self.x - hinge.x, 2) + pow(self.y - hinge.y, 2))
        force = -netMoment / momentArm
        # force = self.mass * momentArm * g
        return force


class Strut():
    instances = []
    # pass in the part mount as the part and vehicle mount as veh

    def __init__(self, part, veh, force):
        if part == veh:
            raise Exception("Vehicle mount and part mount are the same.")
        if veh.fixed != 'fixed':
            raise Exception("Vehicle mount must be a fixed point.")
        for strut in self.instances:
            if strut.veh == veh or strut.part == part:
                raise Exception("Attempting to declare strut with shared mount(s).")
        self.part = part
        self.veh = veh
        self.force = force
        self.instances.append(self)

    def __str__(self):
        return f'Vehicle mount at {self.veh} Part mount at {self.part} Force of {self.force}.'

    def length(self):
        vector = [self.veh.x - self.part.x, self.veh.y - self.part.y]
        scalar = math.sqrt(vector[0] ** 2 + vector[1] ** 2)
        return scalar

    def angle(self):
        vector = [self.part.x - self.veh.x, self.part.y - self.veh.y]
        hypotenuse = math.sqrt(vector[0] ** 2 + vector[1] ** 2)
        xRatio = vector[0]/hypotenuse
        yRatio = vector[1]/hypotenuse
        return xRatio, yRatio

    def torque(self, hinge):
        momentArm = [[self.part.x-hinge.x], [self.part.y-hinge.y]]
        xRatio, yRatio = self.angle()
        force = utils.interpolate_array_positions(array=self.force, position=self.length())
        forceComponents = [[force * xRatio], [force * yRatio]]
        momentSum = momentArm[1][0]*forceComponents[0][0] - momentArm[0][0]*forceComponents[1][0]
        return momentSum


# # L1H1 high mass
# importFile = '1280N_strut_data.csv'
# hinge = location(x=5090.5/1000, y=2299.3/1000, fixed='hinge')
# partCOG = Mass(x=5200/1000, y=1676/1000, fixed='free', mass=51.7)
# accCog = Mass(x=0, y=50/1000, fixed='free', mass=0)
# vehMount = location(x=5175.3/1000, y=1731.6/1000, fixed='fixed')
# partMount = location(x=5111.6/1000, y=2149/1000, fixed='free')
# handle = Handle(x=5291.2/1000, y=914.5/1000, fixed='free')

# L2H2 high mass
importFile = '1355N_strut_data.csv'
hinge = location(x=5662/1000, y=2572/1000, fixed='hinge')
partCOG = Mass(x=5848/1000, y=1837/1000, fixed='free', mass=60.5)
vehMount = location(x=5813.5/1000, y=1823/1000, fixed='fixed')
partMount = location(x=5706/1000, y=2370.8/1000, fixed='free')
handle = Handle(x=5941.2/1000, y=914.5/1000, fixed='free')

# declaring a strut as a linked list means the definitions point to the underlying *Mounts
forceArray = import_csv(importFile)
strut = Strut(part=partMount, veh=vehMount, force=forceArray)
number_of_struts = 2
torqueResult = {'anglePosition': [],
                'COGTorque': [],
                'strutTorque': [],
                'netTorque': []}
handleForce = []

angle = 0
while angle < 70:
    strutTorque = 0
    COGTorque = 0
    for strutIterator in Strut.instances:
        strutIterator.part.rotate(hinge=hinge, angle=1)
        strutTorque += number_of_struts * strutIterator.torque(hinge)
    torqueResult['strutTorque'].append(strutTorque)
    for COGIterator in Mass.instances:
        COGIterator.rotate(hinge=hinge, angle=1)
        COGTorque += COGIterator.torque(hinge)
    handle.rotate(hinge=hinge, angle=1)
    torqueResult['COGTorque'].append(COGTorque)
    netTorque = strutTorque+COGTorque
    torqueResult['netTorque'].append(netTorque)
    angle += 1
    torqueResult['anglePosition'].append(angle)
    handleForce.append(handle.force(hinge=hinge, netMoment=netTorque))

custom_plot(torqueResult=torqueResult, handleForce=handleForce)

t.end()
